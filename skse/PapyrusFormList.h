#pragma once

#include "GameTypes.h"
#include "PapyrusArgs.h"

class BGSListForm;
class VMClassRegistry;

namespace papyrusFormList
{
	void RegisterFuncs(VMClassRegistry* registry);

	VMResultArray<TESForm*> ToArray(BGSListForm * list);
}