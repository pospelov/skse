#include "GameCamera.h"
#include "NiNodes.h"

void LocalMapCamera::SetDefaultStateMinFrustumDimensions(float width, float height)
{
	defaultState->minFrustumWidth = width / 2.0;
	defaultState->minFrustumHeight = height / 2.0;
}

void LocalMapCamera::SetAreaBounds(NiPoint3 * maxBound, NiPoint3 * minBound)
{
	areaBoundsMin = *minBound;
	areaBoundsMax = *maxBound;
	areaBoundsMax.z += (*g_mapLocalHeight);
}

void LocalMapCamera::SetDefaultStateMaxBound(NiPoint3 * maxBound)
{
	defaultState->someBoundMax = *maxBound;
	defaultState->someBoundMax.z += (*g_mapLocalHeight);
}

void LocalMapCamera::SetDefaultStateBounds(float x, float y, float z)
{
	defaultState->someBoundMin.x = x - defaultState->someBoundMax.x;
	defaultState->someBoundMin.y = y - defaultState->someBoundMax.y;
	defaultState->someBoundMin.z = z - defaultState->someBoundMax.z;
}

NiCamera * TESCamera::GetNiCamera() const
{
	NiCamera *camera = nullptr;
	std::size_t size = cameraNode->m_children.m_emptyRunStart;
	for (std::size_t i = 0; i < size; ++i)
	{
		NiAVObject* pObj = cameraNode->GetAt(i);
		if (!pObj)
			continue;

		if (pObj->GetRTTI() == NiRTTI_NiCamera) {
			camera = (NiCamera*)pObj;
			break;
		}

	}
	return camera;
}

bool PlayerCamera::WorldPtToScreenPt3(const NiPoint3 &in, float &x_out, float &y_out, float &z_out, float zeroTolerance)
{
	if (!cameraNode)
		return false;

	NiCamera *camera = GetNiCamera();
	if (!camera)
		return false;

	return camera->WorldPtToScreenPt3(const_cast<NiPoint3 *>(&in), &x_out, &y_out, &z_out, zeroTolerance);
}